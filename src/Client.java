
public class Client {
	private int timpSosire;
	private int timpProcesare;
	private int timpAsteptare;
	private int idClient;
	
	public Client(int tSosire,int tProcesare,int iClient) {
		this.setTimpSosire(tSosire);
		this.setTimpProcesare(tProcesare);
		this.setIdClient(iClient);
	}

	public int getTimpSosire() {
		return timpSosire;
	}

	public void setTimpSosire(int timpSosire) {
		this.timpSosire = timpSosire;
	}

	public int getTimpProcesare() {
		return timpProcesare;
	}

	public void setTimpProcesare(int timpProcesare) {
		this.timpProcesare = timpProcesare;
	}

	public int getTimpAsteptare() {
		return timpAsteptare;
	}

	public void setTimpAsteptare(int timpAsteptare) {
		this.timpAsteptare = timpAsteptare;
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	
	

}
