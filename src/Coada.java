import java.util.ArrayList;

public class Coada extends Thread {
	private int timpAsteptare;
	private int timpAsteptareCoada;
	private int timpCoadaGoala=0;
	private int timpPlecare=0;
	private ArrayList<Client>coada= new ArrayList<Client>();
	
	public void run() {
		while(true) {
			if(coada.isEmpty()==false) {
				try {
					sleep(coada.get(0).getTimpProcesare()*1000);
					timpAsteptare=coada.get(0).getTimpAsteptare()+timpPlecare;
					Supermarket.setTimpAsteptareMediu(timpAsteptare);
					timpPlecare=Supermarket.setTimpPlecareClient(coada.get(0).getIdClient(),timpPlecare);
					Supermarket.adaugaTextLog("Clientul "+coada.get(0).getIdClient()+" asteapta "+timpAsteptare+" minute si  pleaca la minutul "
					+ timpPlecare+"\n");
				}catch (InterruptedException e) {
					e.printStackTrace();
				}finally {
					coada.remove(0);
				}
		     	}
			else{
				timpCoadaGoala=timpCoadaGoala+1;
				try{
					sleep(1000);
				} catch(InterruptedException e){	
					e.printStackTrace();
			      }
			}
		}
	}

	public int clientiPeCoada() {
		return coada.size();
	}

	public int getTimpCoadaGoala() {
		return timpCoadaGoala;
	}

	public void adaugaClient(Client client) {
		coada.add(client);
	}

	public Client getClientCurent(int nrClient){
		return coada.get(nrClient);
	}

	public int getTimpProcesareCasa() {
		int timp = 0;
		for(int i=0;i<coada.size();i++) {
			timp=timp+coada.get(i).getTimpProcesare();
		}
		return timp;
	}
}
