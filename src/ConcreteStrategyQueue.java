
public class ConcreteStrategyQueue {
	public static Coada[] cMarket;
	private int nCozi;

	public ConcreteStrategyQueue(Coada[] coziMarket, int numarCozi) {
		cMarket= new Coada[numarCozi];
		for( int i=0;i<numarCozi;i++){
			cMarket[i]=new Coada(); 	
		}
		 for(int i=0;i<numarCozi;i++) {
			 cMarket[i]=coziMarket[i]; 		 
		 }
		  nCozi=numarCozi;
	}

	public int pozitieOptima() {
		int pozitie= 0;	
		if(cMarket.length > 1){
			int dimensiuneMinimaCoada=cMarket[0].clientiPeCoada();
			for( int i=1; i<nCozi; i++){
				int dimensiuneCurentaCoada =cMarket[i].clientiPeCoada();
				 if (dimensiuneCurentaCoada < dimensiuneMinimaCoada ){
					 dimensiuneMinimaCoada=dimensiuneCurentaCoada;
					 pozitie=i;
				 }
			}
		}
		 return pozitie;
	}


}
