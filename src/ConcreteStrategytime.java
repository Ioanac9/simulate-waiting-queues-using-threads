public class ConcreteStrategytime {
	public static Coada[] cMarket;
	private int nCozi;

	public ConcreteStrategytime(Coada[] coziMarket, int numarCozi) {
		cMarket= new Coada[numarCozi];
		for( int i=0;i<numarCozi;i++){
			cMarket[i]=new Coada(); 	
		}
		 for(int i=0;i<numarCozi;i++) {
			 cMarket[i]=coziMarket[i]; 		 
		 }
		  nCozi=numarCozi;
	}

	public int pozitieOptima() {
		int pozitie= 0;	
		if(cMarket.length > 1){
			int timpMinim=cMarket[0].getTimpProcesareCasa();
			for( int i=1; i<nCozi; i++){
				int timpCurentServire =cMarket[i].getTimpProcesareCasa();
				 if ( timpCurentServire < timpMinim ){
					 timpMinim=timpCurentServire;
					 pozitie=i;
				 }
			}
		}
		 return pozitie;
	}


}
