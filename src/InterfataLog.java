
import java.awt.*;
import javax.swing.*;

public class InterfataLog extends JPanel {
 
JTextArea _resultArea = new JTextArea(6, 20);
private static JTextArea logTxt;

 public InterfataLog(JTextArea logTxt2) {
	  logTxt = new JTextArea();
	 _resultArea=logTxt2;
	 this.add(logTxt2);
     JScrollPane scrollingArea = new JScrollPane(_resultArea);
     this.setLayout(new BorderLayout());
     this.add(scrollingArea, BorderLayout.CENTER);
    _resultArea.setEditable(false);
  
 }

public static JTextArea getLogTxt() {
	return logTxt;
}

public static void setLogTxt(JTextArea logTxt) {
	InterfataLog.logTxt = logTxt;
}
 
}