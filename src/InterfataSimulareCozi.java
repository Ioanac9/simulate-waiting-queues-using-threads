

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class InterfataSimulareCozi extends JPanel{
public InterfataSimulareCozi(){}
public void paintComponent(Graphics g) {
	super.paintComponent(g);
	int coordonataX = 0;
	for(int i=0; i<Supermarket.coziMarket.length; i++) {			
		int coordonataY = 0;
		desenCasa(g, coordonataX, coordonataY,i);		
		for(int j=0; j<Supermarket.coziMarket[i].clientiPeCoada(); j++){
			coordonataY=coordonataY +25;			
			int id = Supermarket.coziMarket[i].getClientCurent(j).getIdClient() + 1;	
			desenClient(g, coordonataX + 20, coordonataY, Color.orange, id);	
			}	
		coordonataX=coordonataX + 100;
	}	
}
private void desenCasa(Graphics g, int x, int y,int i){
	g.setColor(Color.black);
	g.fillRect(x, y, 60, 20);
	g.setColor(Color.white);
	g.drawString("Coada"+(i+1), x+10, y+15);
	this.repaint();
}

private void desenClient(Graphics g, int x, int y, Color c, int clientId){
	g.setColor(c);
	g.fillOval(x, y, 25, 20);
	g.setColor(Color.black);
	g.drawString( Integer.toString(clientId-1) , x+8, y+12);
	this.repaint();
}

public void updateThis()
{
	this.repaint();

}
		
}
