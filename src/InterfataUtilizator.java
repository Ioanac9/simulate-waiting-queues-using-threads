
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class InterfataUtilizator extends JFrame {
	private static JTextArea logTxt = new JTextArea();
	private static final long serialVersionUID = 1L;
	private static InterfataLog newFrame ;
	private JPanel pane = new JPanel(new GridBagLayout());
	public static int maxProcessingTime = 60;
	public static int minProcessingTime = 1;
	public static int maxArrivingTime = 60;
	public static int minArrivingTime = 1;
	public static int numbersOfServers = 6;
	public static int numberOfClients = 100;
	public static int timeLimit = 60;
	public InterfataLog interfataLog;
	GridBagConstraints c = new GridBagConstraints();
	JScrollPane scrollpane;
	Supermarket m;
	String validare;
	String s;
	
	private static JTextField textTimp=new JTextField(5);
	private JTextField textNumarCase= new JTextField(5);
	private JTextField textNumarClienti=new JTextField(5);
	private JTextField textMinSosire=new JTextField(5);
	private JTextField textMaxSosire=new JTextField(5);
	private JTextField textMinServire=new JTextField(5);
	private JTextField textMaxServire=new JTextField(5);
	private JTextField textSimulare=new JTextField(5);
	private JTextField textValidare=new JTextField(30);
	JLabel lNumarCase = new JLabel("Numar case");
	JLabel lNumarClienti = new JLabel("Numar clienti");
	JLabel lMinimSosire = new JLabel("Timp minim sosire");
	JLabel lMaximSosire = new JLabel("Timp maxim sosire");
	JLabel lMinimServire = new JLabel("Timp minim servire");
	JLabel lMaximServire = new JLabel("Timp maxim servire");
	JLabel lTimpSimulare = new JLabel("Timp simulare");
	JLabel lTimpReal = new JLabel("Timp real");
	JLabel lLog = new JLabel("Log");
	JLabel lValidare = new JLabel("Observatii");
	JLabel lStrategie = new JLabel("Alege strategia de simulare");
	JButton b1 = new JButton("Start");
	String[] strategy = { "Shortest_time", "Shortest_queue" };
	JComboBox strategyList;
	static String alegereStrategie;
	
		public InterfataUtilizator() {
			super("Simulare Coada");      
		pane.setLayout(new BoxLayout(pane, BoxLayout.PAGE_AXIS));
			 strategyList = new JComboBox(strategy);
			strategyList.setSelectedIndex(1);
			strategyList.addActionListener(new ActionListener(){
				 public void actionPerformed(ActionEvent e) {	
		
		        JComboBox cb = (JComboBox)e.getSource();
		        String strg = (String)cb.getSelectedItem();
		       alegereStrategie=strg;
		    }
			});
			
			c.gridx = 0;	c.gridy = 0;
			pane.add(lNumarCase,c);
			c.gridx = 1;c.gridy = 0;
			pane.add( textNumarCase, c);
			s = Integer.toString(numbersOfServers) ;
			textNumarCase.setText(s);
			
			c.gridx = 0;	c.gridy = 1;
			pane.add(lNumarClienti,c);
			c.gridx = 1;c.gridy = 1;
			pane.add( textNumarClienti, c);
			s = Integer.toString(numberOfClients) ;
			textNumarClienti.setText(s);
			
			c.gridx = 0;	c.gridy = 2;
			pane.add(lMinimSosire,c);
			c.gridx = 1;	c.gridy = 2;
			pane.add( textMinSosire,c);
			s =Integer.toString(minArrivingTime) ;
			 textMinSosire.setText(s);
					
			c.gridx = 0;	c.gridy = 3;
			pane.add(lMaximSosire,c);
			c.gridx = 1;	c.gridy = 3;
			pane.add( textMaxSosire,c);
			s =Integer.toString(maxArrivingTime) ;
			 textMaxSosire.setText(s);
			
			c.gridx = 0;	c.gridy = 4;
			pane.add( lMinimServire,c);
			c.gridx = 1;	c.gridy = 4;
			pane.add( textMinServire,c);
			s =Integer.toString(minProcessingTime) ;
			 textMinServire.setText(s);
			
			c.gridx = 0;	c.gridy = 5;
			pane.add( lMaximServire,c);
			c.gridx = 1;	c.gridy = 5;
			pane.add( textMaxServire,c);
			s = Integer.toString(maxProcessingTime);
			 textMaxServire.setText(s);
			
			c.gridx = 0;	c.gridy = 6;
			pane.add( lTimpSimulare,c);
			c.gridx = 1;	c.gridy = 6;
			pane.add( textSimulare,c);
			s = Integer.toString( timeLimit);
			 textSimulare.setText(s);		
			
			c.gridx = 1;c.gridy = 7;	
			pane.add(b1, c);
			
			c.gridx = 1;c.gridy = 8;
			pane.add(lTimpReal, c);
			c.gridx = 2;c.gridy = 8;
			pane.add(getTextTimp(), c);
			
			c.gridx = 1;c.gridy = 9;
			pane.add(lValidare, c);
			c.gridx = 2;c.gridy = 9;
			pane.add(textValidare,c);
			c.gridx = 1;c.gridy = 10;
			pane.add(lStrategie,c);
			c.gridx = 2;c.gridy = 10;
			
			pane.add(strategyList,c);
				
			b1.addActionListener(new ActionListener(){
				 public void actionPerformed(ActionEvent arg0) {
					 validare=verificareDateIntrare();	
						if (validare!=null) {
							textValidare.setText(validare);	
							 textValidare.setForeground(Color.white);
							 textValidare.setBackground(Color.red);	
							}
							else
							{
								textValidare.setText("Date valide");
							    textValidare.setForeground(Color.white);
							    textValidare.setBackground(Color.green);
							    createNewFrame();
							}
				}		
			});
			this.add(pane);
			Toolkit tk = Toolkit.getDefaultToolkit();
		    Dimension screenSize = tk.getScreenSize();
			 int screenHeight = screenSize.height;
			 int screenWidth = screenSize.width;	   
			 this.setLocation(screenWidth/2 , screenHeight / 6);
	}

	private String verificareDateIntrare()
	{String s = null;
		
		if(textNumarCase.getText().isEmpty() || textNumarClienti.getText().isEmpty() || textMinSosire.getText().isEmpty() || 
				textMaxSosire.getText().isEmpty() || textMinServire.getText().isEmpty()|| textMaxServire.getText().isEmpty() ||
				textSimulare.getText().isEmpty())
			s="Exista campuri goale in tabela";
			else { 
				if( Integer.parseInt(textMinSosire.getText()) > Integer.parseInt(textSimulare.getText())||Integer.parseInt(textMaxSosire.getText())
						>Integer.parseInt(textSimulare.getText()) ){
					   s="Timpul de sosire este mai mare decat cel de simulare";
				}
				else
				{
					if(Integer.parseInt(textMinServire.getText())>Integer.parseInt(textSimulare.getText())||Integer.parseInt(textMaxServire.getText()
							) >  Integer.parseInt(textSimulare.getText()) ){
						s="Timpul de servire este mai mare decat cel de simulare";
					}
					else
					{
						if( Integer.parseInt(textMinServire.getText()) >  Integer.parseInt(textMaxServire.getText()) )
							s="Timp minim de servire > Timp maxim de servire";
						else
						{
							 if( Integer.parseInt(textMinSosire.getText()) >  Integer.parseInt(textMaxSosire.getText()) )
								 s="Timp minim de sosire > Timp maxim de sosire";
						}
					}
				}
			}
		if( s==null) {

			Supermarket supermarket = new Supermarket(Integer.parseInt(textNumarClienti.getText()),Integer.parseInt(textNumarCase.getText()),
					Integer.parseInt(textMinSosire.getText()), Integer.parseInt(textMaxSosire.getText()),Integer.parseInt(textMinServire.getText()),
					Integer.parseInt(textMaxServire.getText()),Integer.parseInt(textSimulare.getText()));
			supermarket.start();
		}
		return s;
	}
	

public static  void createNewFrame() {
    JFrame graficLog = new JFrame();			
    graficLog.setSize(650, 500);
	Supermarket.setTextLog("");
	graficLog.setTitle("Simulare log");
	graficLog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    graficLog.add(newFrame= new InterfataLog(getLogTxt()));
	graficLog.setVisible(true);
}
public void setLogTxt(JTextArea logTxt) {
	this.logTxt = logTxt;
}

public static JTextField getTextTimp() {
	return textTimp;
}

public void setTextTimp(JTextField textTimp) {
	this.textTimp = textTimp;
}

public static JTextArea getLogTxt() {
	return logTxt;
}

public static void main(String[] args) {

	JFrame demo = new InterfataUtilizator();
	demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	demo.pack();
	demo.setVisible(true);

}

}
