import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;

public class Supermarket extends Thread {
	private static ArrayList<Client>listaClienti=new ArrayList<Client>();
	public static Coada[] coziMarket;
	public static int timpCozi;
	private static int timpAsteptare=0;
	private static int timpAsteptareMediu=0;
	
	public static int getTimpAsteptareMediu() {
		return timpAsteptareMediu;
	}

	public static void setTimpAsteptareMediu(int timpAsteptareMediu) {
		Supermarket.timpAsteptareMediu = Supermarket.timpAsteptareMediu+timpAsteptareMediu;
	}

	private static  int timpPlecareClient;
	private static int timpSosireClient;
	private static String textLog="";
	private int numarCozi=2;
	private int numarClienti=2;
	private int timpSimulare=60;
	private int timpMinProcesare=1;
	private int timpMaxProcesare=60;
	private int timpMediuProcesare=0;
	private int timpMinSosire=1;
	private int timpMaxSosire=60;
	private int numarMaxClienti=100;
	private int oraFull;
	private int timpTotalCoziGoale=0;

	ConcreteStrategyQueue strategieCoada;
	ConcreteStrategytime strategieTimp;
	JFrame graficCozi;
	private Random numarRandom=new Random();
	private InterfataLog logTextInterfata;
	private InterfataSimulareCozi coziAnimateInterfata;
	
	public Supermarket(int nrClienti,int nrCozi,int tMinSosire,int tMaxSosire,int tMinProc,int tMaxProc,int tSimulare) {
	
		coziMarket=(new Coada[nrCozi]);
		for(int i=0;i<nrCozi;i++) {
			coziMarket[i]=new Coada();
		}	
		this.numarClienti=nrClienti;
		this.numarCozi=nrCozi;
		this.timpMinSosire=tMinSosire;
		this.timpMaxSosire=tMaxSosire;
		this.timpMinProcesare=tMinProc;
		this.timpMaxProcesare=tMaxProc;
		this.timpSimulare=tSimulare;	
	}
	
	public static int setTimpPlecareClient(int nrIdentificare,int timp ) {
		for(Client client:listaClienti) {
			if(client.getIdClient()==nrIdentificare) {
				timpPlecareClient=client.getTimpSosire()+client.getTimpProcesare()+client.getTimpAsteptare()+timp;
			}
		}
		return timpPlecareClient;
	}
	
	public void fullOra() {
		int nrClientiCurenti=0;
		for(int i=0;i<numarCozi;i++) {
			nrClientiCurenti=nrClientiCurenti+coziMarket[i].clientiPeCoada();
		}
		if(nrClientiCurenti>numarMaxClienti) {
			numarMaxClienti=nrClientiCurenti;
			oraFull=timpCozi;
		}				
	}
	
	public int getTimpMinProcesare() {
		return timpMinProcesare;
	}

	public void setTimpMinProcesare(int timpMinProcesare) {
		this.timpMinProcesare = timpMinProcesare;
	}

	public int getTimpMaxProcesare() {
		return timpMaxProcesare;
	}

	public void setTimpMaxProcesare(int timpMaxProcesare) {
		this.timpMaxProcesare = timpMaxProcesare;
	}

	public int getTimpMinSosire() {
		return timpMinSosire;
	}

	public void setTimpMinSosire(int timpMinSosire) {
		this.timpMinSosire = timpMinSosire;
	}

	public int getTimpMaxSosire() {
		return timpMaxSosire;
	}

	public void setTimpMaxSosire(int timpMaxSosire) {
		this.timpMaxSosire = timpMaxSosire;
	}

	public void timpCoziGoale() {
		for(int i=0;i<numarCozi;i++) {
			 timpTotalCoziGoale=timpTotalCoziGoale+getCoziMarket()[i].getTimpCoadaGoala();	
		}
	}
	
	synchronized public static String getTextLog() {
		return textLog;
	}
	
	synchronized public static void setTextLog(String s){
		textLog = s;
		InterfataUtilizator.getLogTxt().setText(textLog);
	}

	synchronized public static void adaugaTextLog(String s) {
		textLog=textLog+s;
	}
	
	public static int getTimpCase(){
		return timpCozi;
	}
	public static void adaugareTimpAsteptare(int timp) {
		timpAsteptare =timpAsteptare+ timp;
	}

	public static int getTimpAsteptare() {
		return timpAsteptare;
	}
	

	public void generareClienti() {
		listaClienti.clear();
		if(1<=timpMinSosire && timpMaxSosire<=60) {
			for(int i=1;i<=numarClienti;i++) {
				int timpSosireGenerat=timpMinSosire+numarRandom.nextInt(timpMaxSosire-timpMinSosire);
				int timpProcesareGenerat=timpMinProcesare+ numarRandom.nextInt(timpMaxProcesare-timpMinProcesare);
				listaClienti.add(new Client(timpSosireGenerat,timpProcesareGenerat,i));
			}
		}

		
		Toolkit tk = Toolkit.getDefaultToolkit();
	    Dimension screenSize = tk.getScreenSize();
	    int screenHeight = screenSize.height;
	    int screenWidth = screenSize.width;	   
		graficCozi= new JFrame();
		graficCozi.setSize(300, 400);
		graficCozi.setTitle("Simulare Grafica");
		graficCozi.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    	graficCozi.add(coziAnimateInterfata = new  InterfataSimulareCozi());
		graficCozi.setVisible(true);
		graficCozi.setLocation(screenWidth / 2, screenHeight / 70);

		 for(Coada coada:coziMarket){
			 coada.start();
		 }
		 
	}
	
	 private void afisareStatistici() {
		 String s= new String("Timp de simulare: "
	 +timpSimulare+"\nTimp de asteptare mediu: "+ (float) (timpAsteptareMediu /numarClienti) + "\nTimp de servire mediu: "
	 + (float) (timpMediuProcesare /numarClienti) +"\nTimp case libere: "+(float) (timpTotalCoziGoale / numarCozi)+ "\nTimpul"
	 + " de varf: "+ oraFull);
		 Supermarket.adaugaTextLog("\n\n"+s);
		 InterfataUtilizator.getLogTxt().setText(Supermarket.getTextLog());		
		}

	public void run(){
		 int m=0;
		 generareClienti();
		 for( timpCozi=0; timpCozi<=timpSimulare; timpCozi++){		
			 InterfataUtilizator.getTextTimp().setText(Integer.toString(timpCozi));
			 for(Client client: listaClienti){
				 if(client.getTimpSosire() == timpCozi){
					 if (InterfataUtilizator.alegereStrategie =="Shortest_queue"){
						 strategieCoada=new ConcreteStrategyQueue(coziMarket, numarCozi);
						  m=strategieCoada.pozitieOptima();
						  coziMarket[m].adaugaClient(client);
						  timpMediuProcesare = timpMediuProcesare + client.getTimpProcesare();					 
					 }
					 else {
						 strategieTimp=new ConcreteStrategytime(coziMarket,numarCozi);
						   m=strategieTimp.pozitieOptima();	
						   coziMarket[m].adaugaClient(client);
						   timpMediuProcesare = timpMediuProcesare + client.getTimpProcesare();	 
					 }
					 Supermarket.adaugaTextLog("Clientul " +client.getIdClient()+" ajunge la "+ "minutul "+client.getTimpSosire()
	 					+ " ,este adaugat la coada  "+ (m+1) +" iar durata cumparaturilor de procesat este de  "+client.getTimpProcesare()+
	 					" minute"+"\n");		 
				 }
			 }
			 fullOra();
			 coziAnimateInterfata.updateThis();
			 try {
				 InterfataUtilizator.getLogTxt().setText(textLog);
					sleep(2000);
			 }catch (InterruptedException e) {
					e.printStackTrace();
			 }
		 }
		 timpCoziGoale();
		 afisareStatistici();
	}

	public static Coada[] getCoziMarket() {
		return coziMarket;
	}

	public static void setCoziMarket(Coada[] coziMarket) {
		Supermarket.coziMarket = coziMarket;
	}	
}
