import java.util.ArrayList;


import junit.framework.TestCase;

public class TestApplication extends TestCase {
	int numarClienti=3;
	int numarCase=2;
	int timpMinSosire=1;
	int timpMaxSosire=3;
	int timpMinServire=1;
	int timpMaxServire=2;
	int timpSimulare=5;
	
	public void testSimulareNULL() {
		
	  Supermarket sp=new Supermarket(numarClienti,numarCase,timpMinSosire,timpMaxSosire=0,
			  timpMinServire, timpMaxServire,timpSimulare);
	 
	         sp.start();
	    
   
 	assertNull(sp );
 	
	}
	public void testSimulareEgalitate() {
		
		  Supermarket sp=new Supermarket(numarClienti,numarCase,timpMinSosire,timpMaxSosire=0,
				  timpMinServire, timpMaxServire,timpSimulare);
		  Supermarket sp2=new Supermarket(numarClienti,numarCase,timpMinSosire,timpMaxSosire=0,
				  timpMinServire, timpMaxServire,timpSimulare);
		         sp.start();
		         sp2.start();
	   
	 	assertEquals(sp,sp2);

}
	public void testGenerareRandom() {

		 // Supermarket sp=new Supermarket(3,2,1,3,1,4,10);
		  Supermarket sp=new Supermarket(numarClienti,numarCase,timpMinSosire,timpMaxSosire,
				  timpMinServire, timpMaxServire,timpSimulare);
		  sp.start();
			
		 assertEquals( sp.getTimpMaxProcesare(),2);
	}
}


